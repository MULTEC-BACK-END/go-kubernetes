package main

import (
	"net/http"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
)

var dir, _ = filepath.Abs(filepath.Dir(os.Args[0]))

func setupRouter() *gin.Engine {
	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"messages": "Hello World"})
	})

	r.GET("/env", func(c *gin.Context) {
		host := os.Getenv("HOST_DBX")
		username := os.Getenv("USERNAME_DBX")
		password := os.Getenv("PASSWORD_DBX")
		port := os.Getenv("PORT_DBX")
		c.JSON(http.StatusOK, gin.H{"host": host, "username": username, "password": password, "port": port})
	})

	r.Static("/public", dir+"/assets")

	return r
}

func main() {
	r := setupRouter()
	r.Run(":5544")
}
