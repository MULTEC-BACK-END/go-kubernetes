# builder
FROM golang:1-alpine AS builder
RUN apk add git ca-certificates --update

ENV SERVICE_NAME deploymen
ENV APP /src/${SERVICE_NAME}/
ENV WORKDIR ${GOPATH}${APP}

WORKDIR $WORKDIR
ADD . $WORKDIR

RUN go build

###############################################################

#image
FROM alpine
RUN apk add ca-certificates --update

ENV SERVICE_NAME deploymen
ENV APP /src/${SERVICE_NAME}/
ENV GOPATH /go
ENV WORKDIR ${GOPATH}${APP}

COPY --from=builder ${WORKDIR}${SERVICE_NAME} $WORKDIR

CMD ${WORKDIR}${SERVICE_NAME}